package com.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Advertisement.class)
public abstract class Advertisement_ {

	public static volatile SingularAttribute<Advertisement, BigDecimal> cost;
	public static volatile SingularAttribute<Advertisement, Heading> heading;
	public static volatile SingularAttribute<Advertisement, Author> author;
	public static volatile SingularAttribute<Advertisement, String> name;
	public static volatile SingularAttribute<Advertisement, Integer> id;
	public static volatile SingularAttribute<Advertisement, String> text;
	public static volatile SingularAttribute<Advertisement, LocalDate> publicationDate;

	public static final String COST = "cost";
	public static final String HEADING = "heading";
	public static final String AUTHOR = "author";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String TEXT = "text";
	public static final String PUBLICATION_DATE = "publicationDate";

}

