package com;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Heading.class)
public abstract class Heading_ {

	public static volatile ListAttribute<Heading, Advertisement> advertisements;
	public static volatile SingularAttribute<Heading, String> name;
	public static volatile SingularAttribute<Heading, Integer> id;

	public static final String ADVERTISEMENTS = "advertisements";
	public static final String NAME = "name";
	public static final String ID = "id";

}

