package com;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Email.class)
public abstract class Email_ {

	public static volatile SingularAttribute<Email, Author> author;
	public static volatile SingularAttribute<Email, String> name;
	public static volatile SingularAttribute<Email, Integer> id;

	public static final String AUTHOR = "author";
	public static final String NAME = "name";
	public static final String ID = "id";

}

