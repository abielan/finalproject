package com.controller;

import com.domain.Author;
import com.service.impl.AuthorServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("authors")
public class AuthorController {
    private final AuthorServiceImpl authorServiceImpl;

    @Autowired
    public AuthorController(AuthorServiceImpl authorServiceImpl) {
        this.authorServiceImpl = authorServiceImpl;
    }


    @PostMapping(value = "/writer")
    public void save(@RequestBody Author author) {
        authorServiceImpl.save(author);
    }

    @PutMapping(value = "/writer")
    public void update(@RequestBody Author author) {
        authorServiceImpl.update(author);
    }

    @DeleteMapping(value = "/writer/{id}")
    public void deleteById(@PathVariable("id") int authorId) {
        authorServiceImpl.deleteById(authorId);
    }

    @GetMapping(value = "/writer/{id}")
    public Author findById(@PathVariable("id") int id) {
        return authorServiceImpl.findById(id);
    }

}
