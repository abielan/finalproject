package com.controller;

import com.domain.Heading;
import com.service.impl.HeadingServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("headings")
public class HeadingController {
    private final HeadingServiceImpl headingServiceImpl;

    @Autowired
    public HeadingController(HeadingServiceImpl headingServiceImpl) {
        this.headingServiceImpl = headingServiceImpl;
    }


    @PostMapping(value = "/headline")
    public void save(@RequestBody Heading heading) {
        headingServiceImpl.save(heading);
    }

    @PutMapping(value = "/headline")
    public void update(@RequestBody Heading heading) {
        headingServiceImpl.update(heading);
    }

    @DeleteMapping(value = "/headline/{id}")
    public void deleteById(@PathVariable("id") int headingId) {
        headingServiceImpl.deleteById(headingId);
    }

    @GetMapping(value = "/headline/{id}")
    public Heading findById(@PathVariable("id") int id) {
        return headingServiceImpl.findById(id);

    }

}
