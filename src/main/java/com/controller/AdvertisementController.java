package com.controller;

import com.domain.Advertisement;
import com.service.impl.AdvertisementServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("advertisements")
public class AdvertisementController {
    private final AdvertisementServiceImpl advertisementServiceImpl;

    @Autowired
    public AdvertisementController(AdvertisementServiceImpl advertisementServiceImpl) {
        this.advertisementServiceImpl = advertisementServiceImpl;
    }

    @PostMapping(value = "/announcement")
    public void save(@RequestBody Advertisement advertisement) {
        advertisementServiceImpl.save(advertisement);
    }

    @PutMapping(value = "/announcement")
    public void update(@RequestBody Advertisement advertisement) {
        advertisementServiceImpl.update(advertisement);
    }

    @DeleteMapping(value = "/announcement/{id}")
    public void deleteById(@PathVariable("id") int advertisemenId) {
        advertisementServiceImpl.deleteById(advertisemenId);
    }

    @GetMapping(value = "/announcement/{id}")
    public Advertisement findById(@PathVariable("id") int id) {
        return advertisementServiceImpl.findById(id);

    }


}