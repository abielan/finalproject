package com.controller;

import com.domain.SuitableAd;
import com.service.impl.SuitableAdServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("suitable-ads")
public class SuitableAdController {
    private final SuitableAdServiceImpl suitableAdServiceImpl;

    @Autowired
    public SuitableAdController(SuitableAdServiceImpl suitableAdServiceImpl) {
        this.suitableAdServiceImpl = suitableAdServiceImpl;
    }

    @PostMapping(value = "/suitable-ad")
    public void save(@RequestBody SuitableAd suitableAd) {
        suitableAdServiceImpl.save(suitableAd);
    }

    @PutMapping(value = "/suitable-ad")
    public void update(@RequestBody SuitableAd heading) {
        suitableAdServiceImpl.update(heading);
    }

    @GetMapping(value = "/suitable-ad/{id}")
    public SuitableAd findById(@PathVariable("id") int id) {
        return suitableAdServiceImpl.findById(id);

    }

    @DeleteMapping(value = "/suitable-ad/{id}")
    public void deleteById(@PathVariable("id") int headingId) {
        suitableAdServiceImpl.deleteById(headingId);
    }



}
