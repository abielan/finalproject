package com.dao;

import com.domain.Advertisement;

import java.util.List;

public interface EmailDAO {
    List<String> findAllEmailsForSending(Advertisement adv);
}
