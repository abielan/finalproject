package com.dao;

import com.domain.Advertisement;
import com.domain.Email;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class MySQLEmailDAO implements EmailDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<String> findAllEmailsForSending(Advertisement adv) {


        TypedQuery<String> query = em.createQuery(
                "SELECT e.name " +
                "FROM SuitableAd s " +
                "JOIN s.author au " +
                "JOIN au.email e " +
                "JOIN s.heading h " +
                "WHERE h.id = :h_id " +
                " AND :price BETWEEN s.priceFrom AND s.priceTo", String.class);

        query.setParameter("h_id", adv.getHeading().getId());
        query.setParameter("price", adv.getCost());

        List<String> list = query.getResultList();

        return list;
    }


}
