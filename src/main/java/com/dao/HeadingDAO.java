package com.dao;

import com.domain.Heading;

import javax.persistence.EntityManager;

public interface HeadingDAO extends CrudDAO<Heading> {
    Heading findById(int id);
}
