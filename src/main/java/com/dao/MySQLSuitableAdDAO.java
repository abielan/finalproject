package com.dao;

import com.domain.SuitableAd;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class MySQLSuitableAdDAO implements SuitableAdDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public void save(SuitableAd suitableAd) {
        em.persist(suitableAd);

    }

    public SuitableAd findById(int id) {
        return em.find(SuitableAd.class, id);
    }

    @Override
    public void deleteById(int id) {
        SuitableAd suitableAd = em.getReference(SuitableAd.class, id);
        em.remove(suitableAd);

    }

    @Override
    public void update(SuitableAd suitableAd) {
        SuitableAd mergeSuitableAd = em.merge(suitableAd);
        em.persist(mergeSuitableAd);
    }
}
