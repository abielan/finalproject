package com.dao;


import com.domain.Advertisement;
import com.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Transactional
public class MySQLAdvertisementDAO implements AdvertisementDAO {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private EmailService emailService;

    @Override
    public void save(Advertisement advertisement) {
        em.persist(advertisement);
        emailService.sendAd(advertisement);
    }

    public Advertisement findById(int id) {
        return em.find(Advertisement.class, id);
    }




    @Override
    public void deleteById(int id) {
        Advertisement advertisement = em.getReference(Advertisement.class, id);
        em.remove(advertisement);

    }

    @Override
    public void update(Advertisement advertisement) {
        Advertisement mergeAdvertisement = em.merge(advertisement);
        em.persist(mergeAdvertisement);
    }

    @Override
    public void deleteInactiveAds() {
        TypedQuery<Advertisement> query = em.createQuery("FROM Advertisement a WHERE a.isActive = false", Advertisement.class);

        query.executeUpdate();
    }


}
