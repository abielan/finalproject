package com.dao;

import com.domain.SuitableAd;

public interface SuitableAdDAO  extends CrudDAO<SuitableAd>{
    SuitableAd findById(int id);
}
