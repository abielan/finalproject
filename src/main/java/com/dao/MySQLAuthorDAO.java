package com.dao;

import com.Author_;
import com.domain.Author;
import com.domain.Author;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.SingularAttribute;

@Repository
public class MySQLAuthorDAO implements CrudDAO<Author> {
    @PersistenceContext
    private EntityManager em;

    @Override
    public void save(Author author) {

        author.getPhone().setAuthor(author);
        author.getEmail().setAuthor(author);

        em.persist(author);

    }

    public Author findById(int id) {
        return em.find(Author.class, id);
    }

    @Override
    public void deleteById(int id) {
        Author author = em.getReference(Author.class, id);
        em.remove(author);

    }

    @Override
    public void update(Author author) {
        Author mergeAuthor = em.merge(author);
        em.persist(mergeAuthor);
    }

}
