package com.dao;

import com.domain.Heading;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Repository
public class MySQLHeadingDAO implements HeadingDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public void save(Heading heading) {
        em.persist(heading);

    }

    public Heading findById(int id) {
        return em.find(Heading.class, id);
    }

    @Override
    public void deleteById(int id) {
        Heading heading = em.getReference(Heading.class, id);
        em.remove(heading);

    }

    @Override
    public void update(Heading heading) {
        Heading mergeHeading = em.merge(heading);
        em.persist(mergeHeading);
    }
}
