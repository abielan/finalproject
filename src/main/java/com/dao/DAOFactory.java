package com.dao;

import com.domain.Heading;

public abstract class DAOFactory {
    public static final int MY_SQL = 1;
    public static final int POSTGRE_SQL = 2;

    public abstract AuthorDAO getAuthorDAO();

    public abstract AdvertisementDAO getAdvertisementDAO();

    public abstract HeadingDAO getHeadingDAO();

    public static DAOFactory getFactory(int id) {
        switch (id) {
            case MY_SQL:
                return new MySQLDAOFactory();

            default:
                return new MySQLDAOFactory();
        }
    }


}
