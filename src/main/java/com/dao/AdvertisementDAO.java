package com.dao;

import com.domain.Advertisement;

import java.util.List;


public interface AdvertisementDAO extends CrudDAO<Advertisement> {
    void deleteInactiveAds();

    Advertisement findById(int id);

}
