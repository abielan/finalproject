package com.service;

import com.domain.Advertisement;

import java.util.List;

public interface EmailService {
    List<String> findAllEmailsForSending(Advertisement adv);
    void sendAd(Advertisement advertisement);

}
