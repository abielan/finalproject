package com.service.impl;

import com.domain.Heading;
import com.repository.HeadingRepository;
import com.service.HeadingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class HeadingServiceImpl implements HeadingService {

    @Autowired
    private HeadingRepository headingRepository;


    public Heading findById(int id) {
        return headingRepository.findById(id);
    }

    @Override
    public void save(Heading heading) {
        headingRepository.save(heading);
    }

    @Override
    public void update(Heading heading) {
        headingRepository.save(heading);
    }


    @Override
    public void deleteById(int id) {
        headingRepository.deleteById(id);
    }
}
