package com.service.impl;

import com.domain.Author;
import com.repository.AuthorRepository;
import com.service.CRUDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional

public class AuthorServiceImpl implements CRUDService<Author> {

    @Autowired
    private AuthorRepository authorRepository;


    public Author findById(int id) {
        return authorRepository.findById(id);
    }

    @Override
    public void save(Author author) {

        authorRepository.save(author);
    }

    @Override
    public void update(Author author) {
        authorRepository.save(author);
    }


    @Override
    public void deleteById(int id) {
        authorRepository.deleteById(id);
    }
}
