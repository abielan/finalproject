package com.service.impl;

import com.domain.Advertisement;
import com.repository.AdvertisementRepository;
import com.service.AdvertisementService;
import com.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AdvertisementServiceImpl implements AdvertisementService {

    @Autowired
    private AdvertisementRepository advertisementRepository;

    @Autowired
    private EmailService emailService;

    public Advertisement findById(int id) {
        return advertisementRepository.findById(id);
    }


    @Override
    public void save(Advertisement advertisement) {
        advertisementRepository.save(advertisement);
        emailService.sendAd(advertisement);
    }

    @Override
    public void update(Advertisement advertisement) {
        advertisementRepository.save(advertisement);
    }

    @Override
    public void deleteById(int id) {
        advertisementRepository.deleteById(id);
    }

    //Fire at 00:00 on the last day of every month
    @Scheduled(cron = "0/55 * * * * *")
    public void deleteInactiveAds() {
        advertisementRepository.deleteInactiveAds();
    }
}

//Author list ads ;

/*
 * ads: [
 *
 *
 * {
 *   id: 3
 * },
 * {
 *   id: 4
 * },
 * {
 *   "id":0
 * }
 *
 * ]
 *
 * */