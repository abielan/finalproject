package com.service.impl;

import com.dao.SuitableAdDAO;
import com.domain.SuitableAd;
import com.repository.SuitableAdRepository;
import com.service.SuitableAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SuitableAdServiceImpl implements SuitableAdService{

    @Autowired
    private SuitableAdRepository suitableAdRepository;

    public SuitableAd findById(int id) {
        return suitableAdRepository.findById(id);
    }



    @Override
    public void save(SuitableAd suitableAd) {

        suitableAdRepository.save(suitableAd);
    }

    @Override
    public void update(SuitableAd suitableAd) {
        suitableAdRepository.save(suitableAd);
    }


    @Override
    public void deleteById(int id) {
        suitableAdRepository.deleteById(id);
    }
}


