package com.service.impl;

import com.dao.EmailDAO;
import com.domain.Advertisement;
import com.repository.EmailRepository;
import com.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EmailServiceImpl implements EmailService {

    @Autowired
    private EmailDAO emailDAO;

    @Autowired
    private EmailRepository emailRepository;

    @Autowired
    private JavaMailSender sender;

    @Override
    public List<String> findAllEmailsForSending(Advertisement adv) {
        return emailRepository.findAllEmailsForSending(adv);
    }

    public void sendAd(Advertisement advertisement) {

        List<String> emails = findAllEmailsForSending(advertisement);

        SimpleMailMessage message = new SimpleMailMessage();

        for (String email : emails) {
            message.setTo(email);
            message.setSubject("Hello, from Spring");
            message.setText("Welcome");

            sender.send(message);
        }


    }
}
