package com.service;

import com.domain.SuitableAd;

public interface SuitableAdService extends CRUDService<SuitableAd> {
    SuitableAd findById(int id);
}
