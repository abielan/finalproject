package com.service;

import com.domain.Heading;



public interface HeadingService extends CRUDService<Heading> {

    Heading findById(int id);
}
