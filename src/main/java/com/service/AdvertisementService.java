package com.service;

import com.domain.Advertisement;

public interface AdvertisementService extends CRUDService<Advertisement> {
    void deleteInactiveAds();
}
