package com.repository;

import com.domain.Heading;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface HeadingRepository  extends JpaRepository<Heading, Integer> {
    Heading findById(int id);

    void deleteById (int id);



}
