package com.repository;

import com.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository  extends JpaRepository<Author, Integer> {

    Author findById(int id);

    void deleteById (int id);


}
