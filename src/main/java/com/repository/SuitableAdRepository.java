package com.repository;

import com.domain.SuitableAd;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuitableAdRepository extends JpaRepository<SuitableAd, Integer> {
    SuitableAd findById(int id);

    void deleteById (int id);

}
