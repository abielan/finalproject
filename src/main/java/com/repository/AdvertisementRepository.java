package com.repository;

import com.domain.Advertisement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface AdvertisementRepository extends JpaRepository<Advertisement, Integer> {

    Advertisement findById(int id);

    void deleteById (int id);

    @Query(value = "DELETE FROM Advertisement  WHERE isActive = false", nativeQuery = true)
    @Modifying
    void deleteInactiveAds();
}

