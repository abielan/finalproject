package com.repository;

import com.domain.Advertisement;
import com.domain.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmailRepository extends JpaRepository<Email, Integer> {

    @Query("SELECT e.name " +
            "FROM SuitableAd s " +
            "JOIN s.author au " +
            "JOIN au.email e " +
            "JOIN s.heading h " +
            "WHERE h.id = :#{#adv.heading.id} AND :#{#adv.cost} BETWEEN s.priceFrom AND s.priceTo")
    List<String> findAllEmailsForSending(@Param(value = "adv") Advertisement adv);
}
