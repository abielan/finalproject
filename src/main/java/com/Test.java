//package com;
//
//import com.domain.*;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityManagerFactory;
//import javax.persistence.EntityTransaction;
//import javax.persistence.Persistence;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.temporal.ChronoUnit;
//import java.util.ArrayList;
//import java.util.*;
//import java.util.List;
//
//public class Test {
//    public static void main(String[] args) {
//        EntityManagerFactory factory = Persistence.createEntityManagerFactory("final_project");
//
//        EntityManager em = factory.createEntityManager();
//
//        EntityTransaction transaction = em.getTransaction();
//
//        transaction.begin();
//
//        Heading estate = new Heading("Estate");
//        Heading cars = new Heading("Cars");
//
//        Advertisement buy = new Advertisement("Buy");
//        Advertisement sell = new Advertisement("Sell");
//
//        estate.addAd(buy);
//        cars.addAd(sell);
//
//        buy.setHeading(estate);
//        sell.setHeading(cars);
//
//        em.persist(estate);
//        em.persist(cars);
//
//        Author ivan = new Author("Ivan");
//        Author andrew = new Author("Andrew");
//
//        buy.setAuthor(andrew);
//        sell.setAuthor(ivan);
//
//        ivan.addAd(buy);
//        andrew.addAd(sell);
//
//        Phone phoneAndrew = new Phone("122");
//        Phone phoneIvan = new Phone("345");
//
//        ivan.setPhone(phoneIvan);
//        andrew.setPhone(phoneAndrew);
//
//        Email emailAndrew = new Email("Andrew@ukr.net");
//        Email emailIvan = new Email("Ivan@ukr.net");
//
//        ivan.setEmail(emailIvan);
//        andrew.setEmail(emailAndrew);
//
//        emailIvan.setAuthor(ivan);
//        emailAndrew.setAuthor(andrew);
//
//        Address dnepr = new Address("Dnepr");
//        Address kyiv = new Address("Kyiv");
//
//        ivan.addAddress(dnepr);
//        andrew.addAddress(kyiv);
//
//        em.persist(ivan);
//        em.persist(andrew);
//
//        transaction.commit();
//
//        em.close();
//        factory.close();
//
//
//
//
//    }
//
//
//
//}
//
