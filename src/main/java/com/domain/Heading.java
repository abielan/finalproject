package com.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Heading {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "heading_id")
    private int id;

    @Version
    private int version;

    @Column(unique = true)
    private String name;

    public Heading() {
    }

    public Heading(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

}

//Ad1, Ad2, Ad3, Ad4(0)
